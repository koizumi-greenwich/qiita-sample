<?php
// パラメータの取得
$param = file_get_contents("php://input");
$param = json_decode($param);
if (!isset($param->index)) {
    http_response_code(404);
    exit;
}

// データの読み込み
$json = file_get_contents("10-data.json");
$data = json_decode($json, true);
if (!isset($data['products'][$param->index])) {
    http_response_code(404);
    exit;
}

// 出力
header("Content-Type: application/json");
echo json_encode($data['products'][$param->index]);
